import cv2

cap=cv2.VideoCapture(1)


classifier=cv2.CascadeClassifier('cascade.xml')

while True: 
    _,frame=cap.read()
    gray=cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
    objects=classifier.detectMultiScale(gray,5,91,minSize=(70,70))

    for (x,y,w,h) in objects:
        cv2.rectangle(frame,(x,y),(x+w,y+h),(0,255,0),2)
        cv2.putText(frame,'Object',(x,y-10),cv2.FONT_HERSHEY_SIMPLEX,1,(0,255,0),2)

    cv2.imshow('frame',frame)

    k=cv2.waitKey(12) & 0xFF
    if k==ord('q'):
        break
cap.release()
cv2.destroyAllWindows()
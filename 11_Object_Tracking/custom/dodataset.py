import cv2 
import numpy
import imutils
import os 

folder='positive'

if not os.path.exists(folder):
    os.makedirs(folder)
    print('Folder created')

cap = cv2.VideoCapture(1)

x1,y1= 230,150
x2,y2= x1+200,y1+200

images=0

while cap.isOpened():
    _,frame = cap.read()
    if frame is None:
        break
    AuxFrame = frame.copy()
    frame = cv2.flip(frame,1)
    object=AuxFrame[y1:y2,x1:x2]
    object=imutils.resize(object,width=40)

    cv2.rectangle(frame,(x1,y1),(x2,y2),(0,255,0),2)
    cv2.imshow('frame',frame)
    cv2.imshow('object',object)

    k = cv2.waitKey(12) & 0xFF
    if k == ord('q'):
        break
    elif k == ord('s'):
        cv2.imwrite(folder+'/'+str(images)+'.jpg',object)
        images=images+1

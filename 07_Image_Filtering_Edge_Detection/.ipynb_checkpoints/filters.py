import cv2 
import numpy 


feature_params = dict( maxCorners = 100,
                       qualityLevel = 0.1,
                       minDistance = 15)

def filters(filters, image):
    if filters == "blur":
        image = cv2.blur(image, (13,13))
    elif filters == "canny":
        image = cv2.Canny(image, 80, 150)
    elif filters == "features":
        image = image
        frame_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        corners = cv2.goodFeaturesToTrack(frame_gray,**feature_params)
        corners = numpy.int0(corners)
        
        for corner in corners:
            x, y = corner.ravel()
            cv2.circle(image, (x, y), 5, (255, 0, 0), -1)


        for i in range(len(corners)):
	        for j in range(i + 1, len(corners)):
                    corner1 = tuple(corners[i][0])
                    corner2 = tuple(corners[j][0])
                    color = tuple(map(lambda x: int(x), numpy.random.randint(0, 255, size=3)))
                    cv2.line(image, corner1, corner2, color, 1)     
    elif filters=="black":
        image=cv2.cvtColor(image,cv2.COLOR_RGB2GRAY)
    else:
        image=image

                
    return image
def main():
    filtro = "preview"
    source = cv2.VideoCapture(1)
    while True:
        _, frame = source.read()
        image=filters(filters=filtro,image=frame)
        cv2.imshow("Camera Filters", image)
        key = cv2.waitKey(1)
        if key == ord('Q') or key == ord('q') or key == 27:
            break
        elif key == ord('C') or key == ord('c'):
            filtro = 'canny'
        elif key == ord('B') or key == ord('b'):
            filtro = 'blur'
        elif key == ord('F') or key == ord('f'):
            filtro = "features"
        elif key == ord('W') or key == ord('w'):
            filtro= "black"
        elif key == ord('P') or key == ord('p'):
            filtro = 'preview'

        
if __name__ == "__main__":
    main()
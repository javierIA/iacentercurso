from re import I
from time import gmtime
import cv2 
import numpy
import random
params=dict( maxCorners=100, qualityLevel=0.1, minDistance=15, blockSize=9)
def filters(filters,image):
    if filters=='preview':
        image=image
    elif filters=='canny':
        image=cv2.Canny(image,90,150)
    elif filters=='blur':
        image=cv2.blur(image,(13,13))
    elif filters=='white':
        image=cv2.cvtColor(image,cv2.COLOR_RGB2GRAY)
    elif filters=='features':
        imagegray=cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
        corners=cv2.goodFeaturesToTrack(imagegray,**params)
        corners=numpy.int0(corners)
        for coner in corners:
            x,y=coner.ravel()
            cv2.circle(image,(x,y),5,(255,0,0),-1)

        for i in range(len(corners)):
            for j in range(i+1,len(corners)):
                coner1=tuple(corners[i][0])
                coner2=tuple(corners[j][0])
                r,g,b=random.randint(0,255),random.randint(0,255),random.randint(0,255)
                cv2.line(image,coner1,coner2,(r,g,b),1)

    return image


def main2():
    img = cv2.imread('07_Image_Filtering_Edge_Detection\lena.png')
    img = filters('blur', img)
    cv2.imshow('lena', img)
    cv2.waitKey(0)

def main():
    filtro="preview"
    cap=cv2.VideoCapture(1)
    while cap.isOpened():
        _,frame=cap.read()
        result=filters(filtro,frame)
        cv2.imshow("Camera Filters",result)
        key=cv2.waitKey(1)
        if key==ord('Q') or key==ord('q') or key==27:
            break
        elif key==ord('C') or key==ord('c'):
            filtro='canny'
        elif key==ord('B') or key==ord('b'):
            filtro='blur'
        elif key==ord('P') or key==ord('p'):
            filtro='preview'
        elif key == ord('W') or key == ord('w'):
            filtro = 'white'
        elif key == ord('F') or key == ord('f'):
            filtro = "features"

if __name__ == '__main__':
    main()
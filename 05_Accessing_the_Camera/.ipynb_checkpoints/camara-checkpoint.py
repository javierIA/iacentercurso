import cv2
import numpy as np

camera=cv2.VideoCapture(1)
cv2.waitKey(12) #add only if you want to skip the first 12 frames of the video is the camera is not ready
while camera.isOpened(): # Escape

    ret,frame = camera.read()
    
    if not ret :
        print("Can't receive frame (stream end?). Exiting ...")
        continue
    cv2.rectangle(frame,(100,100),(300,300),(0,255,0),2)
    cv2.circle(frame,(200,200),100,(0,0,255),2)
    cv2.line(frame,(100,100),(300,300),(255,0,0),2)
    cv2.putText(frame,"Hello World",(100,100),cv2.FONT_HERSHEY_SIMPLEX,1,(255,255,255),2)
    cv2.imshow("frame",frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    if cv2.waitKey(1) & 0xFF == ord('w'):
        cv2.imwrite('image.png',frame)


camera.release()
cv2.destroyAllWindows()
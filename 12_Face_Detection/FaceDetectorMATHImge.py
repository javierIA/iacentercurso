from sys import maxsize
import cv2
from matplotlib.pyplot import gray 
import numpy as np


img=cv2.imread("image.jpg",1)
gray=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

classifier=cv2.CascadeClassifier("haarcascade_frontalface_default.xml")

faces=classifier.detectMultiScale(gray,scaleFactor=1.1,minNeighbors=5,minSize=(32,32),maxSize=(400,400))

for (x,y,w,h) in faces:
    cv2.rectangle(img,(x,y),(x+w,y+h),(0,255,0),2)

cv2.imshow("Image",img)

cv2.waitKey(0)